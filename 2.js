// Ejercicio 2
// Dada una matriz de N elementos repetidos,
// crea una función numbersTop para obtener los tres elementos más repetidos ordenados de forma descendente por número de repeticiones.

function numbersTop(array) {
  const repeatedValues = array.filter((value, index, self) => self.indexOf(value) === index)
  const repeatedValuesCount = repeatedValues.map(value => {
    return {
      value,
      count: array.filter(arrayValue => arrayValue === value).length
    }
  })
  const repeatedValuesCountSorted = repeatedValuesCount.sort((a, b) => b.count - a.count)
  return repeatedValuesCountSorted.map(value => value.value).slice(0, 3)
}

/**
 * TEST Ejercicio 2
 */
console.log(numbersTop([3, 3, 1, 4, 1, 3, 1, 1, 2, 2, 2, 3, 1, 3, 4, 1])) // [ 1, 3, 2 ]
console.log(numbersTop(['a', 3, 2, 'a', 2, 3, 'a', 3, 4, 'a', 'a', 1, 'a', 2, 'a', 3])) // [ 'a', 3, 2 ]
