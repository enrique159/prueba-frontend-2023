// Ejercicio 1
// Dada una matriz de N elementos en la que todos los elementos son iguales excepto uno,
// crea una función findUniq que retorne el elemento único.

function unique (value, index, self) {
  return self.indexOf(value) !== index
}

function findUniq(array) {
  const repeatedValues = array.filter(unique)
  return array.filter(value => !repeatedValues.includes(value))
}


/**
 * TEST Ejercicio 1
 */
console.log(findUniq(['12', 10, '12', 11, 1, 11, 10, '12'])) // 1
console.log(findUniq(['Capitán América', 'Hulk', 'Deadpool', 'Capitán América', 'Hulk', 'Wonder Woman', 'Deadpool', 'Iron Man', 'Iron Man'])) // 'Wonder Woman'
